from django.test import TestCase
from . import views


# Create your tests here.
class TestViews(TestCase):
    def test_index_ok(self):
            response = self.client.get('/conciertos/')
            self.assertEqual(response.status_code, 200)

    def test_get_musicos_ok(self):
            response = self.client.get('/conciertos/musicos/')
            self.assertEqual(response.status_code, 200)

    def test_get_grupos_ok(self):
            response = self.client.get('/conciertos/grupos/')
            self.assertEqual(response.status_code, 200)

    def test_index_view(self):
        response = self.client.get('/conciertos/')
        self.assertEqual(response.resolver_match.func, views.index)

    def test_musicos_view(self):
        response = self.client.get('/conciertos/grupos/')
        self.assertEqual(response.resolver_match.func, views.grupos)

    def test_grupos_view(self):
        response = self.client.get('/conciertos/musicos/')
        self.assertEqual(response.resolver_match.func, views.musicos)
